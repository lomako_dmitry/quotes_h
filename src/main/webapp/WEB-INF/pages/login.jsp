<%@include file="/WEB-INF/include/page.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <title>Auth</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/css/style.css"/>
</head>
<body style="background-color: #ddd;">

<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered" style="width: 230px;">
        <c:if test="${not empty sessionScope.loginError}">
            <p class="error-message"><spring:message code="login.form.loginError"/></p>
            <c:remove var="loginError" scope="session"/>
        </c:if>

        <c:if test="${not empty sessionScope.fieldError}">
            <p class="error-message"><spring:message code="error.fieldError"/></p>
            <c:remove var="fieldError" scope="session"/>
        </c:if>

        <form:form method="post" action="${ctx}/login">
            <p>
                <input type="text" name="login" value="" style="width: 100%"
                       placeholder="<spring:message code="form.name"/>" autofocus/>
            </p>
            <p>
                <input type="password" name="password" value="" style="width: 100%"
                       placeholder="<spring:message code="form.password"/>"/>
            </p>
            <input style="float: left" type="submit" value="<spring:message code="login.text"/>"/>
        </form:form>
        <div style="clear: both"></div>
    </div>
</div>

</body>
</html>