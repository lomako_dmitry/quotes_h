<%@include file="/WEB-INF/include/page.jsp" %>

<!doctype html>
<html>
<head>
    <title>Add</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="quote" value="${requestScope.quote}"/>
    <div class="quote">
        <div class="quote-info">
            <div class="rating-info">
                <input type="hidden" value="${quote.id}">
                <c:if test="${empty sessionScope.voted[quote.id]}">
                    <button class="rating-down"> -</button>
                </c:if>
                <span class="rating">${quote.rating}</span>
                <c:if test="${empty sessionScope.voted[quote.id]}">
                    <button class="rating-up"> +</button>
                </c:if>
                <span class="right"><fmt:formatDate value="${quote.date}" pattern="dd.MM.yyyy HH:mm:ss"/></span>
            </div>
        </div>
        <hr>
        <div class="quote-text">
            <c:out value="${quote.text}"/>
        </div>
        <hr>
        <div class="author">
            <c:out value="${quote.author}"/>
        </div>
        <sec:authorize access="isAuthenticated() and hasRole('ROLE_ADMIN')">
            <form:form class="adm-btn" method="get" action="${ctx}/quote/edit/${quote.id}">
                <input type="submit" value="<spring:message code="quote.edit"/>">
            </form:form>
            <form:form class="adm-btn" method="post" action="${ctx}/quote/remove">
                <input type="hidden" value="${quote.id}" name="quote-id">
                <input type="submit" value="<spring:message code="quote.delete"/>">
            </form:form>
        </sec:authorize>
        <div style="clear: both"></div>
    </div>

    <sec:authorize access="isAuthenticated()">
        <form:form method="post" action="${ctx}/comment/add">
            <p><input type="hidden" name="quote-id" value="${quote.id}"></p>

            <p><textarea rows="4" cols="70" name="text" maxlength="200"></textarea></p>

            <p><input type="submit" value="<spring:message code="comment.add"/>"></p>
        </form:form>
    </sec:authorize>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><spring:message code="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>

    <spring:message code="quotes.comments"/> (${fn:length(quote.comments)})
    <hr>

    <c:if test="${not empty quote.comments}">
        <c:forEach var="comment" items="${quote.comments}">
            <div class="comment">
                <div class="comment-info">
                    <span>${comment.user.name}</span>
                    <span class="right"><fmt:formatDate value="${comment.date}" pattern="dd.MM.yyyy HH:mm:ss"/></span>
                </div>
                <hr>
                <div class="comment-text">
                    <c:out value="${comment.text}"/>
                </div>
                <sec:authorize access="isAuthenticated() and hasRole('ROLE_ADMIN')">
                    <hr>
                    <form:form class="adm-btn" method="get" action="${ctx}/comment/edit/${comment.id}">
                        <input type="submit" value="<spring:message code="comment.edit"/>">
                    </form:form>
                    <form:form class="adm-btn" method="post" action="${ctx}/comment/remove">
                        <input type="hidden" value="${comment.id}" name="comment-id">
                        <input type="submit" value="<spring:message code="comment.delete"/>">
                    </form:form>
                </sec:authorize>
            </div>
        </c:forEach>
    </c:if>
</div>
<script>var ctx = "${pageContext.request.contextPath}";</script>
<script>var csrf = "${_csrf.token}";</script>
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
</body>
</html>