<%@include file="/WEB-INF/include/page.jsp" %>

<!doctype html>
<html>
<head>
    <title>Edit quote</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="quote" value="${requestScope.quote}"/>

    <form:form method="post" action="${ctx}/quote/edit/${quote.id}">
        <p><input type="hidden" name="quote-id" value="${quote.id}"></p>
        <p>
            <label><spring:message code="quote.text"/>
                <textarea rows="10" cols="100" name="text" maxlength="500" style="vertical-align: top">
                        ${quote.text}
                </textarea>
            </label>
        </p>
        <p>
            <label><spring:message code="quote.author"/>
                <input type="text" name="author" value="${quote.author}"
                       placeholder="<spring:message code="quote.author"/>">
            </label>
        </p>
        <input type="submit" value="<spring:message code="quote.edit"/>">
    </form:form>
    <form:form method="post" action="${ctx}/quote/remove">
        <input type="hidden" value="${quote.id}" name="quote-id">
        <input type="submit" value="<spring:message code="quote.delete"/>">
    </form:form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><spring:message code="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
</body>
</html>