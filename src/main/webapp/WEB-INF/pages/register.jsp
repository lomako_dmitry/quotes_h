<%@include file="/WEB-INF/include/page.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <title>Auth</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/resources/css/style.css"/>
</head>
<body style="background-color: #ddd;">

<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered">
        <c:if test="${not empty sessionScope.fieldError}">
            <p class="error-message"><spring:message code="error.fieldError"/></p>
            <c:remove var="fieldError" scope="session"/>
        </c:if>

        <c:if test="${not empty sessionScope.duplicateError}">
            <p class="error-message"><spring:message code="error.duplicateError"/></p>
            <c:remove var="duplicateError" scope="session"/>
        </c:if>

        <form:form method="post" action="${ctx}/register">
            <p>
                <input type="text" name="name" value="" style="width: 100%"
                       placeholder="<spring:message code="form.name"/>"/>
            </p>
            <p>
                <input type="password" name="password" value="" style="width: 100%"
                       placeholder="<spring:message code="form.password"/>"/>
            </p>
            <input type="submit" value="<spring:message code="register.form.button"/>"/>
        </form:form>
    </div>
</div>

</body>
</html>