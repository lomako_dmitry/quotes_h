<%@include file="/WEB-INF/include/page.jsp" %>

<!doctype html>
<html>
<head>
    <title>Edit comment</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <c:set var="comment" value="${requestScope.comment}"/>

    <form:form method="post" action="${ctx}/comment/edit/${comment.id}">
        <input type="hidden" name="comment-id" value="${comment.id}">
        <div><spring:message code="comment.author"/> : ${comment.user.name}</div>
        <p>
            <label><spring:message code="quote.text"/>
                <textarea rows="4" cols="100" name="text" maxlength="200">${comment.text}</textarea>
            </label>
        </p>
        <input type="submit" value="<spring:message code="comment.edit"/>">
    </form:form>
    <form:form method="post" action="${ctx}/comment/remove">
        <input type="hidden" value="${comment.id}" name="comment-id">
        <input type="submit" value="<spring:message code="comment.delete"/>">
    </form:form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><spring:message code="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
</body>
</html>