<%@include file="/WEB-INF/include/page.jsp" %>

<!doctype html>
<html>
<head>
    <title>Add quote</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <form:form method="post" action="${ctx}/quote/add">
        <p>
            <label><spring:message code="quote.add.author"/>
                <input type="text" name="author" placeholder="<spring:message code="quote.author"/>">
            </label>
        </p>

        <p>
            <label><spring:message code="quote.add.text"/>
                <textarea rows="10" cols="45" name="text" maxlength="500" style="vertical-align: top"></textarea>
            </label>
        </p>

        <input type="submit" value="<spring:message code="quotes.add.post"/>">
    </form:form>
    <c:if test="${not empty sessionScope.fieldError}">
        <p class="error-message"><spring:message code="error.fieldError"/></p>
        <c:remove var="fieldError" scope="session"/>
    </c:if>
</div>

<script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
<script src="<c:url value="/resources/js/script.js"/>"></script>
</body>
</html>