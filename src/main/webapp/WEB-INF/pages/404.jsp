<%@include file="/WEB-INF/include/page.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <title>Oops, something bad happened</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
    <%--<meta http-equiv="refresh" content="5;URL=<%=request.getContextPath()%>/login.jsp">--%>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered">
        <img src="<c:url value="/resources/img/404.png"/>" alt="404 Error"/>
    </div>
</div>
</body>
</html>