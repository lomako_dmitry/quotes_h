<%@include file="/WEB-INF/include/page.jsp" %>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html>
<head>
    <title>Oops, something bad happened</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>"/>
</head>
<body>
<div id="content">
    <%@include file="/WEB-INF/include/header.jsp" %>

    <div class="centered">
        <h2 class="errorPageInfo">Oops, something bad happened!</h2>
        <p class="errorPageInfo"><%= exception %></p>
        <img src="<c:url value="/resources/img/error.jpg"/>" alt="Error"/>
    </div>
</div>
</body>
</html>