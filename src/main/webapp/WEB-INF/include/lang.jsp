<%@include file="/WEB-INF/include/page.jsp" %>

<c:set var="localeCode" value="${pageContext.response.locale}" />

<form:form action="" method="get">
    <c:forEach items="${paramValues}" var="entry">
        <c:if test="${entry.key != 'locale'}">
            <c:forEach var="value" items="${entry.value}">
                <input type='hidden' name="${entry.key}" value="${value}"/>
            </c:forEach>
        </c:if>
    </c:forEach>

    <select name="locale" onchange="this.form.submit()">
        <c:forEach var="loc" items="${supportedLocales}">
            <option value="${loc}" ${localeCode == loc ? 'selected' : ''}><spring:message code="lang.${loc}"/></option>
        </c:forEach>
    </select>
</form:form>