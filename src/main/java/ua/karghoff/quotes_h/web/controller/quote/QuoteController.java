package ua.karghoff.quotes_h.web.controller.quote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.service.QuoteService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("/quote")
public class QuoteController {
    private static final Integer ZERO = 0;
    public static final String NOT_FOUND_CONTROLLER = "redirect:/404";

    @Autowired
    QuoteService quoteService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String delete(@RequestParam("quote-id") Long id) {
        Quote quote = quoteService.getById(id);
        if (quote == null) {
            return NOT_FOUND_CONTROLLER;
        }
        quoteService.delete(quote);

        return "redirect:/";
    }

    @RequestMapping(value = "/{id:\\d+}", method = RequestMethod.GET)
    public String get(@PathVariable("id") long id,
                      HttpServletRequest request) {

        Quote quote = quoteService.getById(id);

        if (quote == null) {
            return NOT_FOUND_CONTROLLER;
        }
        request.setAttribute("quote", quote);

        return "quote";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String get() {
        return "addQuote";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String get(@RequestParam("text") String text,
                      @RequestParam("author") String author,
                      HttpSession session) {

        User user = (User) session.getAttribute("user");
        Timestamp ts = new Timestamp(new Date().getTime());

        String target;
        if (checkString(text) || checkString(author)) {
            session.setAttribute("fieldError", "error");
            target = "redirect:/add";
        } else {
            Quote q = new Quote();
            q.setText(text);
            q.setUser(user);
            q.setAuthor(author);
            q.setDate(ts);
            q.setRating(0);

            quoteService.insert(q);
            target = "redirect:/";
        }
        return target;
    }

    @ResponseBody
    @RequestMapping(value = "/rating", method = RequestMethod.POST)
    public String changeRating(@RequestParam("quote-id") Long id,
                               @RequestParam("value") Integer value,
                               HttpSession session) {

        Map<Long, Boolean> voted = (Map<Long, Boolean>) session.getAttribute("voted");
        value = value.compareTo(ZERO);

        Quote q = new Quote();
        q.setId(id);

        if (!voted.containsKey(id)) {
            quoteService.changeRating(q, value);
            voted.put(id, true);
        }
        q = quoteService.getById(id);

        return "{\"newRating\":" + q.getRating() + "}";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/edit/{id:\\d+}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id,
                       HttpServletRequest request) {

        Quote quote = quoteService.getById(id);
        if (quote == null) {
            return NOT_FOUND_CONTROLLER;
        }
        request.setAttribute("quote", quote);

        return "editQuote";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/edit/{id:\\d+}", method = RequestMethod.POST)
    public String edit(@RequestParam("text") String text,
                       @RequestParam("author") String author,
                       @RequestParam("quote-id") Long id,
                       HttpSession session) {

        if (checkString(text) || checkString(author)) {
            session.setAttribute("fieldError", "error");
            return "redirect:/quote/edit/" + id;
        }
        Quote quote = quoteService.getById(id);
        if (quote == null) {
            return NOT_FOUND_CONTROLLER;
        }
        quote.setText(text);
        quote.setAuthor(author);
        quoteService.update(quote);

        return "redirect:/quote/" + id;
    }

    private boolean checkString(String str) {
        return str == null || str.trim().isEmpty();
    }
}
