package ua.karghoff.quotes_h.web.controller.quote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.service.QuoteService;

import java.util.List;

@Controller
@RequestMapping("/best")
public class Best {
    private static final int PER_PAGE = 10;

    @Autowired
    QuoteService quoteService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView byRating(@RequestParam(value = "page", required = false) Integer page,
                                 ModelAndView model) {

        if (page == null) {
            page = 0;
        }

        List<Quote> list = quoteService.getSorted("rating", true, page, PER_PAGE);
        long count = quoteService.count();

        boolean hasMore = count > PER_PAGE * (page + 1);
        boolean hasBefore = page > 0;

        model.addObject("list", list);
        if (hasBefore) {
            model.addObject("prevPage", page - 1);
        }
        if (hasMore) {
            model.addObject("nextPage", page + 1);
        }
        model.setViewName("index");
        return model;
    }
}
