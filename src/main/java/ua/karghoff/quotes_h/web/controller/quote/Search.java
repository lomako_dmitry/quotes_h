package ua.karghoff.quotes_h.web.controller.quote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.service.QuoteService;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/search")
public class Search {
    private static final int PER_PAGE = 10;

    @Autowired
    QuoteService quoteService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView byRating(@RequestParam(value = "page", required = false) Integer pageNumber,
                                 @RequestParam(value = "searchQuery", required = false) String searchQuery,
                                 ModelAndView model) {

        if (pageNumber == null) {
            pageNumber = 0;
        }
        model.setViewName("search");

        if (searchQuery == null || searchQuery.trim().isEmpty()) {
            model.addObject("list", Collections.emptyList());
            return model;
        }
        searchQuery = searchQuery.trim();

        Page<Quote> page = quoteService.findByText(searchQuery, pageNumber, PER_PAGE);
        List<Quote> quotes = page.getContent();
        Long count = page.getTotalElements();

        model.addObject("list", quotes);
        model.addObject("count", count);
        if (page.hasPrevious()) {
            model.addObject("prevPage", pageNumber - 1);
        }
        if (page.hasNext()) {
            model.addObject("nextPage", pageNumber + 1);
        }
        return model;
    }
}
