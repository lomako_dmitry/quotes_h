package ua.karghoff.quotes_h.web.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.karghoff.quotes_h.service.exception.DuplicateEntryException;
import ua.karghoff.quotes_h.service.exception.FieldsValidateException;
import ua.karghoff.quotes_h.util.Registration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    @Autowired
    Registration registration;

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(Model model, String error) {
        if (error != null) {
            model.addAttribute("loginError");
        }
        return "login";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public String registration(HttpServletRequest request, HttpSession session) {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String target = "register";
        try {
            registration.register(name, password);
            target = "login";
        } catch (FieldsValidateException e) {
            session.setAttribute("fieldError", "error");
        } catch (DuplicateEntryException e) {
            session.setAttribute("duplicateError", "error");
        }
        return target;
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String registration() {
        return "register";
    }

}
