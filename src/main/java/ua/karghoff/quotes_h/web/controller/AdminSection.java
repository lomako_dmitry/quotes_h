package ua.karghoff.quotes_h.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.service.UserService;

import java.util.List;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
public class AdminSection {
    private static final int PER_PAGE = 10;

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView byRating(@RequestParam(value = "page", required = false) Integer page,
                                 ModelAndView model) {

        if (page == null) {
            page = 0;
        }

        List<User> list = userService.getAll(page, PER_PAGE);
        long count = userService.count();

        boolean hasMore = count > PER_PAGE * (page + 1);
        boolean hasBefore = page > 0;

        model.addObject("list", list);
        if (hasBefore) {
            model.addObject("prevPage", page - 1);
        }
        if (hasMore) {
            model.addObject("nextPage", page + 1);
        }
        model.setViewName("admin");
        return model;
    }
    
    @ResponseBody
    @RequestMapping(value = "/ban", method = RequestMethod.POST)
    public String changeStatus(@RequestParam(name = "name") Long id,
    						 @RequestParam(name = "value") boolean status,
                             HttpSession session) {
    	System.out.println("Getting " + id + status);
        userService.setBlocked(id, status);
        User blockedUser = userService.getById(id);
        return "{\"user\":\"" + blockedUser.getName() + "\", \"blocked\":" + blockedUser.isBlocked() + "}";
        
    }

}
