package ua.karghoff.quotes_h.web.controller.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.karghoff.quotes_h.entity.Comment;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.service.CommentService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;

@Controller
@RequestMapping("/comment")
public class CommentController {
    public static final String NOT_FOUND_CONTROLLER = "redirect:/404";

    @Autowired
    CommentService commentService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String delete(@RequestParam("comment-id") Long id) {
        Comment comment = commentService.getById(id);
        if (comment == null) {
            return NOT_FOUND_CONTROLLER;
        }
        commentService.delete(comment);

        return "redirect:/quote/" + comment.getQuote().getId();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doPost(@RequestParam("quote-id") Long quoteId,
                         @RequestParam("text") String text,
                         HttpSession session) {

        if (checkString(text)) {
            session.setAttribute("fieldError", "error");
        } else {
            User user = (User) session.getAttribute("user");
            Timestamp ts = new Timestamp(new Date().getTime());

            Quote q = new Quote();
            q.setId(quoteId);

            Comment comment = new Comment();
            comment.setUser(user);
            comment.setText(text);
            comment.setDate(ts);
            comment.setQuote(q);

            commentService.insert(comment);
        }
        return "redirect:/quote/" + quoteId;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/edit/{id:\\d+}", method = RequestMethod.POST)
    public String edit(@RequestParam("comment-id") Long id,
                       @RequestParam("text") String text,
                       HttpSession session) {

        if (checkString(text)) {
            session.setAttribute("fieldError", "error");
            return "redirect:/comment/edit/" + id;
        }
        Comment comment = commentService.getById(id);
        if (comment == null) {
            return NOT_FOUND_CONTROLLER;
        }
        comment.setText(text);
        commentService.update(comment);

        return "redirect:/quote/" + comment.getQuote().getId();
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/edit/{id:\\d+}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id,
                       HttpServletRequest request) {
        Comment comment = commentService.getById(id);
        if (comment == null) {
            return NOT_FOUND_CONTROLLER;
        }
        request.setAttribute("comment", comment);

        return "editComment";
    }

    private boolean checkString(String str) {
        return str == null || str.trim().isEmpty();
    }
}
