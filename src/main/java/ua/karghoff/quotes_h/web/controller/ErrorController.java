package ua.karghoff.quotes_h.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController {

    @RequestMapping(path = "/403", method = RequestMethod.GET)
    public ModelAndView forbidden() {
        ModelAndView model = new ModelAndView();
        model.setStatus(HttpStatus.FORBIDDEN);
        model.setViewName("403");

        return model;
    }

    @RequestMapping(path = "/404", method = RequestMethod.GET)
    public ModelAndView notFound() {
        ModelAndView model = new ModelAndView();
        model.setStatus(HttpStatus.NOT_FOUND);
        model.setViewName("404");

        return model;
    }
}
