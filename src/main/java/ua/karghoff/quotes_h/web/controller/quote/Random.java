package ua.karghoff.quotes_h.web.controller.quote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.service.QuoteService;

import java.util.List;

@Controller
@RequestMapping("/random")
public class Random {
    @Autowired
    QuoteService service;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView byRating(ModelAndView model) {

        List<Quote> list = service.getRandomQuotes(10);
        model.addObject("list", list);
        model.setViewName("index");
        return model;
    }
}
