package ua.karghoff.quotes_h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import ua.karghoff.quotes_h.entity.BlockableEntity;

/**
 * Database repository for {@link BlockableEntity} mapped superclass. <br>
 * Provide soft implementation of delete operation.
 */
@NoRepositoryBean
public interface SoftDeleteRepository<E extends BlockableEntity, ID extends Long> extends JpaRepository<E, ID> {

    /**
     * On delete change entity blocked field to {@code true}
     *
     * @param id blocked entity id
     */
	@Transactional
    @Modifying
    @Query("UPDATE #{#entityName} e SET e.blocked = :blocked WHERE e.id = :id")
    void setBlocked(@Param("id") Long id, @Param("blocked") boolean blocked);
}