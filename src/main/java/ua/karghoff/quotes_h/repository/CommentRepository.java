package ua.karghoff.quotes_h.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.karghoff.quotes_h.entity.Comment;

/**
 * Database repository for {@link Comment} entity. <br>
 * Provide basic CRUD operations.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

}