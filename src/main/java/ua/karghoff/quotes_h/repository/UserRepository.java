package ua.karghoff.quotes_h.repository;

import org.springframework.stereotype.Repository;

import ua.karghoff.quotes_h.entity.User;

/**
 * Database repository for {@link User} entity. <br>
 * Provide basic CRUD operations with additional methods.
 */
@Repository
public interface UserRepository extends SoftDeleteRepository<User, Long> {

    /**
     * Get user by given user name.
     *
     * @param name given user name
     * @return user with given name
     */
    User findByName(String name);
}