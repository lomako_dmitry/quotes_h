package ua.karghoff.quotes_h.service;

import ua.karghoff.quotes_h.entity.Comment;

/**
 * Service for {@link Comment} entity.
 */
public interface CommentService extends Service<Comment> {
}
