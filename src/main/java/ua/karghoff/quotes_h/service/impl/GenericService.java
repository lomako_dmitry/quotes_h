package ua.karghoff.quotes_h.service.impl;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import ua.karghoff.quotes_h.entity.BasicEntity;
import ua.karghoff.quotes_h.service.Service;

import java.util.List;

/**
 * Basic service implementation. <br>
 * By default, forward to repository methods.
 *
 * @param <T>
 */
public abstract class GenericService<T extends BasicEntity> implements
		Service<T> {

	public T getById(long id) {
		return getRepository().findOne(id);
	}

	public T insert(T entity) {
		return getRepository().save(entity);
	}

	public void delete(T entity) {
		getRepository().delete(entity.getId());
	}

	public T update(T entity) {
		return getRepository().save(entity);
	}

	public List<T> getAll(int offset, int limit) {
		return getRepository().findAll(getPageRequest(offset, limit))
				.getContent();
	}

	/**
	 * Return {@link PageRequest} object for pagination DB request.
	 *
	 * @param offset
	 *            page index
	 * @param limit
	 *            page size
	 * @return page request object
	 */
	protected PageRequest getPageRequest(int offset, int limit) {
		return new PageRequest(offset, limit);
	}

	public long count() {
		return getRepository().count();
	}

	/**
	 * Checking for the existence of an entity.
	 *
	 * @param id
	 *            entity id
	 * @return true if entity exists, false otherwise
	 */
	public boolean exist(long id) {
		return getRepository().exists(id);
	}

	protected abstract JpaRepository<T, Long> getRepository();

}
