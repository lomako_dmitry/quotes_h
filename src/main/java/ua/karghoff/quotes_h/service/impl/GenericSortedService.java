package ua.karghoff.quotes_h.service.impl;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import ua.karghoff.quotes_h.entity.BasicEntity;
import ua.karghoff.quotes_h.service.SortedService;

import java.util.List;

/**
 * Service with method to get sorted data from database.
 *
 * @param <T> entity type
 */
public abstract class GenericSortedService<T extends BasicEntity> extends GenericService<T> implements SortedService<T> {

    /**
     * Return list of entities with given sort and pagination params.
     *
     * @param columnName name of column for sorting
     * @param reverse    sorting direction, false to ASC, true for DESC
     * @param offset     page index
     * @param limit      page size
     * @return list of entities
     */
    @Override
    public List<T> getSorted(String columnName, boolean reverse, int offset, int limit) {
        return getRepository().findAll(getPageRequest(columnName, reverse, offset, limit)).getContent();
    }

    /**
     * Return {@link Sort} object for data sorting.
     *
     * @param columnName name of column for sorting
     * @param reverse    sorting direction, false to ASC, true for DESC
     * @return Sort object
     */
    protected Sort getSort(String columnName, boolean reverse) {
        Sort.Direction direction = reverse ? Sort.Direction.DESC : Sort.Direction.ASC;
        return new Sort(direction, columnName);
    }

    /**
     * Return {@link PageRequest} object for pagination and sorting DB request.
     *
     * @param columnName name of column for sorting
     * @param reverse    sorting direction, false to ASC, true for DESC
     * @param offset     page index
     * @param limit      page size
     * @return page request object
     */
    protected PageRequest getPageRequest(String columnName, boolean reverse, int offset, int limit) {
        Sort sort = getSort(columnName, reverse);
        return new PageRequest(offset, limit, sort);
    }

    @Override
    protected abstract JpaRepository<T, Long> getRepository();
}
