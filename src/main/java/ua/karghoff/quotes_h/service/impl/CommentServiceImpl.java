package ua.karghoff.quotes_h.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ua.karghoff.quotes_h.entity.Comment;
import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.repository.CommentRepository;
import ua.karghoff.quotes_h.repository.QuoteRepository;
import ua.karghoff.quotes_h.service.CommentService;

/**
 * Service class for {@link Comment} DB entity. <br>
 * Check for parent {@link Quote} existence before save/update/delete operations.
 */
@Component
@Transactional
public class CommentServiceImpl extends GenericSortedService<Comment> implements CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private QuoteRepository quoteRepository;

    /**
     * Insert comment to Database. <br>
     * Check parent Quote existence.
     *
     * @param comment given comment object
     * @return persisted object
     */
    @Override
    public Comment insert(Comment comment) {
        Quote quote = comment.getQuote();
        if (quoteRepository.exists(quote.getId())) {
            return super.insert(comment);
        }
        return null;
    }

    /**
     * Delete comment from Database. <br>
     * Check parent Quote existence.
     *
     * @param comment given comment
     */
    @Override
    public void delete(Comment comment) {
        Quote quote = comment.getQuote();
        if (quoteRepository.exists(quote.getId())) {
            super.delete(comment);
        }
    }

    /**
     * Update comment in Database. <br>
     * Check parent Quote existence.
     *
     * @param comment given comment
     * @return updated comment
     */
    @Override
    public Comment update(Comment comment) {
        Quote quote = comment.getQuote();
        if (quoteRepository.exists(quote.getId())) {
            return super.update(comment);
        }
        return null;
    }

    @Override
    public JpaRepository<Comment, Long> getRepository() {
        return commentRepository;
    }
}
