package ua.karghoff.quotes_h.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import ua.karghoff.quotes_h.entity.Quote;
import ua.karghoff.quotes_h.repository.QuoteRepository;
import ua.karghoff.quotes_h.service.QuoteService;

/**
 * Service class for {@link Quote} DB entity. <br>
 */
@Component
public class QuoteServiceImpl extends GenericSortedService<Quote> implements QuoteService {

    @Autowired
    private QuoteRepository quoteRepository;

    /**
     * Change rating for given Quote.
     *
     * @param quote quote to rating change
     * @param diff  difference between current and new rating
     */
    @Override
    public void changeRating(Quote quote, int diff) {
        if (diff != 0 && quoteRepository.exists(quote.getId())) {
            quoteRepository.changeRating(quote.getId(), diff);
        }
    }

    @Override
    public List<Quote> getRandomQuotes(long count) {
        return quoteRepository.getRandomQuotes(count);
    }

    @Override
    public Page<Quote> findByText(String searchQueryText, int offset, int limit) {
        Quote q = new Quote();
        q.setText(searchQueryText);
        Example<Quote> example = Example.of(q,
                ExampleMatcher.matching().
                        withMatcher("text",
                                matcher -> matcher.contains().ignoreCase())
                        .withIgnorePaths("rating")
        );

        return quoteRepository.findAll(example, getPageRequest(offset, limit));
    }

    @Override
    public JpaRepository<Quote, Long> getRepository() {
        return quoteRepository;
    }
}
