package ua.karghoff.quotes_h.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import ua.karghoff.quotes_h.entity.User;
import ua.karghoff.quotes_h.repository.UserRepository;
import ua.karghoff.quotes_h.service.UserService;

/**
 * Service class for {@link User} DB entity.
 */
@Component
public class UserServiceImpl extends GenericService<User> implements
		UserService {

	@Autowired
	UserRepository userRepository;

	/**
	 * Get user by given user name.
	 *
	 * @param name
	 *            given user name
	 * @return user with given name
	 */
	@Override
	public User getByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	protected JpaRepository<User, Long> getRepository() {
		return userRepository;
	}

	@Override
	public void setBlocked(Long id, boolean blocked) {
		userRepository.setBlocked(id, blocked);
	}
}
