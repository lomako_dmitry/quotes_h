package ua.karghoff.quotes_h.service;

import ua.karghoff.quotes_h.entity.User;

/**
 * Service for {@link User} entity.
 */
public interface UserService extends BlockedService<User>  {

    /**
     * Get user by given user name.
     *
     * @param name given user name
     * @return user with given name
     */
    public User getByName(String name);
}
