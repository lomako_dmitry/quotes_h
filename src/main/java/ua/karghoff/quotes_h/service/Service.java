package ua.karghoff.quotes_h.service;

import ua.karghoff.quotes_h.entity.BasicEntity;

import java.util.List;

/**
 * Basic interface for services.
 *
 * @param <T> Entity type
 */
public interface Service<T extends BasicEntity> {

    T getById(long id);

    T insert(T entity);

    void delete(T entity);

    T update(T entity);

    List<T> getAll(int offset, int limit);

    long count();

    boolean exist(long id);
}
