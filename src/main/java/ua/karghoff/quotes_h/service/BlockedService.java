package ua.karghoff.quotes_h.service;

import ua.karghoff.quotes_h.entity.BlockableEntity;

/**
 * Basic interface for services.
 *
 * @param <T>
 *            Entity type
 */
public interface BlockedService<T extends BlockableEntity> extends Service<T> {

	void setBlocked(Long id, boolean blocked);
}
