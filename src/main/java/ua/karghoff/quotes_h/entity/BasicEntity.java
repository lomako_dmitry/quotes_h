package ua.karghoff.quotes_h.entity;

import javax.persistence.*;

/**
 * Basic class for Database entities.
 */
@MappedSuperclass
public abstract class BasicEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    public BasicEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
