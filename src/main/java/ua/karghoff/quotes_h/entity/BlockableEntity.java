package ua.karghoff.quotes_h.entity;

import javax.persistence.*;

/**
 * Basic class for Database entities.
 */
@MappedSuperclass
public abstract class BlockableEntity extends BasicEntity {

    @Column(name = "blocked")
    protected boolean blocked;

    public BlockableEntity() {
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
