Quotes project with java core and additional technologies.
Using :
- java SE 8
- Spring context for DI
- Spring JPA for work with DB
- Spring MVC for view controller
- JSP and JSTL for visual part
- Spring Security 
    -- @PreAuthorize annotation
    -- <sec:authorize>
    -- Custom Handlers
- ajax for change rating requests
- Spring MVC Internationalization